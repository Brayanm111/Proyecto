
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="librerias/jquery-3.2.1.min.js"></script>
  <script src="js/funciones.js"></script>
<!------ Include the above in your HEAD tag ---------->
</head>
<body>
    







   <div class="container">         
       
            <form id="formulario" class="form-horizontal" role="form">
                <h2>Registro</h2>
                <div class="form-group">
                    <label for="firstName" class="col-sm-3 control-label">Nombre</label>
                    <div class="col-sm-9">
                        <input type="text" name="Nombre" id="Nombre" placeholder="Nombre Completo" class="form-control" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-3 control-label">Numero de documento de Identidad</label>
                    <div class="col-sm-9">
                        <input type="text" name="Documento_Identidad" id="Documento_Identidad" placeholder="Carnet de Identidad" class="form-control" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email* </label>
                    <div class="col-sm-9">
                        <input type="email" name="Email" id="Email" placeholder="Correo Electronico" class="form-control" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Contraseña*</label>
                    <div class="col-sm-9">
                        <input type="password" name="Contraseña" id="Contraseña" placeholder="Contraseña" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                    </div>
                </div>
                <div class="form-group">
                    <label for="birthDate" class="col-sm-3 control-label">Fecha de Nacimiento*</label>
                    <div class="col-sm-9">
                        <input type="date" name="Fecha_Nacimiento" id="Fecha_Nacimiento" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="phoneNumber" class="col-sm-3 control-label">Numero de Telefono </label>
                    <div class="col-sm-9">
                        <input type="phoneNumber" name="Telefono" id="Telefono" placeholder="Numero de Telefono" class="form-control">
                       
                    </div>
                </div>
                <div class="form-group">
                        <label for="Height" class="col-sm-3 control-label">Direccion</label>
                    <div class="col-sm-9">
                        <input type="text" name="Direccion" id="Direccion" placeholder="Direccion" class="form-control">
                    </div>
               
                <div class="form-group">
                   
                    <div class="col-sm-6">
                        <div class="row">
                      
                    </div>
                </div> <!-- /.form-group -->
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">Enviar</button>
                <label for="cuenta" class="col-sm-3 control-label">¿Ya tienes una cuenta? </label>
               <a class="nav-link" href="login.php">Inicia Sesion</a>
            </form> <!-- /form -->
            
        <div class="mt-3" id="respuesta">
        </div> <!-- ./container -->
        
  
<script type="text/javascript">
    $(document).ready(function(){
        $('#guardarnuevo').click(function(){
          Fecha=$('#Fecha').val();
          Nombre=$('#Nombre').val();
          Cantidad=$('#Cantidad').val();
          Descripcion=$('#Descripcion').val();
            agregardatos(Fecha,Nombre,Cantidad,Descripcion);
        });
   
          //ACTUALIZA LOS REGISTROS
          
        $('#actualizadatos').click(function(){
          actualizaDatos();
        });

    });
</script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
        </body>
</html>