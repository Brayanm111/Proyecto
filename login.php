<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.ui.shake.js"></script>
<!------ Include the above in your HEAD tag ---------->
</head>
<body>
<?php
session_start();
if(!empty($_SESSION['login_user']))
{
header('Location: home.php');
}
?>
   <div id="box">
>
<div class="container">
            <form class="form-horizontal" role="form" method="post">
                <h2>Ingresar</h2>
               
                <div class="form-group">
                    
                    <div class="col-sm-9">
                       
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email </label>
                    <div class="col-sm-9">
                        <input type="email" id="email" placeholder="Correo Electronico" class="form-control" name= "email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Contraseña</label>
                    <div class="col-sm-9">
                        <input type="password" id="password" placeholder="Contraseña" class="form-control">
                    </div>
                </div>
               
                <div class="form-group">
                   
                   <div class="col-sm-6">
                       <div class="row">
                     
                   </div>
               </div> <!-- /.form-group -->
               <div class="form-group">
                   <div class="col-sm-9 col-sm-offset-3">
                       
                   </div>
               </div class="From-group"> 
                <button type="submit" class="btn btn-primary btn-block" value="Log In" id="login">Ingresar</button>
               <label for="cuenta" class="col-sm-3 control-label">¿No tienes una cuenta ? </label>
               <a class="nav-link" href="Registro.php">Registrate</a>
   </div>           
</form> 
</div>
</div>
   
<script>
$(document).ready(function() 
{
$('#login').click(function()
{
var username=$("#Email").val();
var password=$("#Contraseña").val();
var dataString = 'Email='+username+'&Contraseña='+password;
if($.trim(username).length>0 && $.trim(password).length>0)
{
$.ajax({
type: "POST",
url: "ajaxLogin.php",
data: dataString,
cache: false,
beforeSend: function(){ $("#login").val('Connecting...');},
success: function(data){
if(data)
{
$("body").load("home.php").hide().fadeIn(1500).delay(6000);
//or
window.location.href = "home.php";
}
else
{
//Shake animation effect.
$('#box').shake();
$("#login").val('Login')
$("#error").html("<span style='color:#cc0000'>Error:</span> Email y Contraseña Incorrectos.");
}
}
});

}
return false;
});

});
</script>
        </body>
</html>