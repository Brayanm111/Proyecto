<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous">

    <title>Hello, world!</title>
</head>

<body>
    <div class="container my-5">
        <h2>Formulario con FetchAPI</h2>
        
        <form id="formulario">
        
        <label for="Nombre" class="col-sm-3 control-label">Nombre*</label>
            <input type="text" name="Nombre_Cliente" placeholder="Nombre" class="form-control my-3">
            <label for="Documento_Identidad" class="col-sm-3 control-label">Carnet de identidad*</label>
            <input type="text" name="Documento_Identidad" placeholder="Documento_Identidad" class="form-control my-3">
            <input type="text" name="Email" placeholder="Email" class="form-control my-3">
            <input type="text" name="Contraseña" placeholder="Contraseña" class="form-control my-3">
            <input type="text" name="Fecha_Nacimiento" placeholder="Fecha_Nacimiento" class="form-control my-3">
            <input type="number" name="Telefono" placeholder="Telefono" class="form-control my-3">
            <input type="text" name="Direccion" placeholder="Descripcion" class="form-control my-3">
          
            <button class="btn btn-primary" type="submit">Enviar</button>
        </form>
        <div class="mt-3" id="respuesta">
            
        
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script>
    //console.log("hola");
document.addEventListener("DOMContentLoaded", function(event) {
    var formulario = document.getElementById('formulario');
    var respuesta = document.getElementById('respuesta');
    console.log(formulario);
    formulario.addEventListener('submit', function(e){
    e.preventDefault();
    console.log('me diste un click')

    var datos = new FormData(formulario);

    console.log(datos)
    console.log(datos.get('Nombre_Cliente'))
    console.log(datos.get('Documento_Identidad'))
    console.log(datos.get('Email'))
    console.log(datos.get('Contraseña'))
    console.log(datos.get('Fecha_Nacimiento'))
    console.log(datos.get('Telefono'))
    console.log(datos.get('Direccion'))

    fetch('http://127.0.0.1:8000/Proyecto/API-PHP/Usuario.php',{
        mode: 'cors',
        method: 'POST',
        body: datos
    })
        .then( res => res.json())
        .then( data => {
            console.log(data)
            if(data === 'error'){
                respuesta.innerHTML = `
                <div class="alert alert-danger" role="alert">
                    Llena todos los campos
                </div>
                `
            }else{
                respuesta.innerHTML = `
                <div class="alert alert-primary" role="alert">
                    ${data}
                </div>
                `
            }
        } ) 
})
  });
  </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
   
</body>

</html>